//Import thu vien express
const express = require("express");
//Khai bao middleware
const userMiddleware = require("../middlewares/userMiddleware");
//import controller
const { getAllUser, createUser, getUserById, updateUser, deleteUser } = require("../controllers/userController");
//Tao ra router
const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL user: ", req.url);

    next();
});
//get all drink
router.get("/users", userMiddleware.getAUserMiddleware, getAllUser)
//create drink
router.post("/users", userMiddleware.postUserMiddleware, createUser);
//get a drink
router.get("/users/:userId", userMiddleware.getAUserMiddleware, getUserById);
//update drink
router.put("/users/:userId", userMiddleware.putUserMiddleware, updateUser);
//delete drink
router.delete("/users/:userId", userMiddleware.deleteUserMiddleware, deleteUser);

module.exports = router;