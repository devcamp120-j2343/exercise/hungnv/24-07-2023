//Import thu vien express
const express = require("express");
//Khai bao middleware
const prizeMiddleware = require("../middlewares/prizeMiddlewares");
//import controller
const { getAllPrize, createPrize, getPrizeById, updatePrize, deletePrize } = require("../controllers/prizeController");
//Tao ra router
const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL prize: ", req.url);

    next();
});
//get all drink
router.get("/prizes", prizeMiddleware.getAllPrizeMiddleware, getAllPrize)
//create drink
router.post("/prizes", prizeMiddleware.postPrizeMiddleware, createPrize);
//get a drink
router.get("/prizes/:prizeId", prizeMiddleware.getAPrizeMiddleware, getPrizeById);
//update drink
router.put("/prizes/:prizeId", prizeMiddleware.putPrizeMiddleware, updatePrize);
//delete drink
router.delete("/prizes/:prizeId", prizeMiddleware.deletePrizeMiddleware, deletePrize);

module.exports = router;