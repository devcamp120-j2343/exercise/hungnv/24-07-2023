//import thư viện mongoose
const mongoose = require('mongoose');

// import User model
const prizeModel = require('../models/prizeModel');

//get all prize
const getAllPrize = async (req, res) => {
    //B1: thu thập dữ liệu
    //B2: kiểm tra
    //B3: thực thi model
    prizeModel.find()
        .then((data) => {
            if (data && data.length > 0) {
                return res.status(200).json({
                    status: "Get all prize sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any prize",
                    data
                })
            }

        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}
//get one prize
const getPrizeById = (req, res) => {
    //B1: thu thập dữ liệu
    var prizeId = req.params.prizeId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(prizeId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    //B3: thực thi model
    prizeModel.findById(prizeId)
        .then((data) => {
            if (data) {
                return res.status(200).json({
                    status: "Get prize by id sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any prize",
                    data
                })
            }

        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}
//create a user
const createPrize = (req, res) => {
    //B1: thu thập dữ liệu
    const { name, description } = req.body;

    //B2: kiểm tra dữ liệu
    if (!name) {
        return res.status(400).json({
            status: "Bad request",
            message: "name is required!"
        })
    }
    if (!description ) {    
        return res.status(400).json({
            status: "Bad request",
            message: "description is required!"
        })
    }

    //B3: thực hiện thao tác model
    let newPrize = {
        _id: new mongoose.Types.ObjectId(),
        name,
        description
    }

    prizeModel.create(newPrize)
        .then((data) => {
            return res.status(201).json({
                status: "Create new prize sucessfully",
                data
            })
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}
//update prize
const updatePrize = (req, res) => {
    //B1: thu thập dữ liệu
    var prizeId = req.params.prizeId;
    const { name, description } = req.body;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(prizeId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    if (!name) {
        return res.status(400).json({
            status: "Bad request",
            message: "name is required!"
        })
    }
    if (!description) {
        return res.status(400).json({
            status: "Bad request",
            message: "description is required!"
        })
    }

    //B3: thực thi model
    let updatePrize = {
        name,
        description
    }

    prizeModel.findByIdAndUpdate(prizeId, updatePrize)
        .then((data) => {
            if (data) {
                return res.status(200).json({
                    status: "Update prize sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any prize",
                    data
                })
            }

        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}
//delete prize
const deletePrize = (req, res) => {
    //B1: Thu thập dữ liệu
    var prizeId = req.params.prizeId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(prizeId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }

    prizeModel.findByIdAndDelete(prizeId)
        .then((data) => {
            if (data) {
                return res.status(200).json({
                    status: "Delete prize sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any user",
                    data
                })
            }

        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })

}

//Export thanh module
module.exports = {
    getAllPrize,
    getPrizeById,
    createPrize,
    updatePrize,
    deletePrize
}